package com.example.latihanlks

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Adapter
import android.widget.AdapterView
import android.widget.EditText
import android.widget.Spinner

class MenuActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)

            findViewById<Spinner>(R.id.dropdown_barang).onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val barang = findViewById<Spinner>(R.id.dropdown_barang).getSelectedItem().toString()
                var harga = 0.0

                if (barang=="TV"){
                    harga= 2000000.0
                }else if (barang=="Speaker"){
                    harga= 750000.0
                }else if (barang=="DVD Player"){
                    harga= 1250000.0
                }else if (barang=="Projector"){
                    harga= 4000000.0
                }

                findViewById<EditText>(R.id.harga).setText(harga.toString())
            }

        }
    }

    fun proses (view: android.view.View) {
        val intent = Intent(this,DetailActivity::class.java)
        intent.putExtra("nama", findViewById<EditText>(R.id.pembeli).text.toString())
        intent.putExtra("barang", findViewById<Spinner>(R.id.dropdown_barang).getSelectedItem().toString())
        intent.putExtra("harga", findViewById<EditText>(R.id.harga).text.toString())
        intent.putExtra("jumlah", findViewById<EditText>(R.id.jumlah).text.toString())
        startActivity(intent)
    }
}