package com.example.latihanlks

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Spinner
import android.widget.TextView
import java.text.NumberFormat
import java.util.*

class DetailActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        var harga = 0.0
        var pembeli = intent.getStringExtra("nama")
        var barang = intent.getStringExtra("barang")
        harga = intent.getStringExtra("harga").toString().toDouble()
        if (barang=="TV"){
            harga= 2000000.0
        }else if (barang=="Speaker"){
            harga= 750000.0
        }else if (barang=="DVD Player"){
            harga= 1250000.0
        }else if (barang=="Projector"){
            harga= 4000000.0
        }

        var jumlah = intent.getStringExtra("jumlah").toString().toDouble()
        var diskon = 0.0
        var total = 0.0
        total = harga * jumlah
        if (total>=10){
            diskon = total * 1/50
            total = total - diskon
        }

        val local = Locale("id","ID")
        val formatter = NumberFormat.getCurrencyInstance(local)

        val hargaformat = formatter.format(harga)
        val diskonformat = formatter.format(diskon)
        val totalformat = formatter.format(total)

        findViewById<TextView>(R.id.buyer).text=": $pembeli"
        findViewById<TextView>(R.id.item).text=": $barang"
        findViewById<TextView>(R.id.price).text=": $hargaformat"
        findViewById<TextView>(R.id.many).text=": $jumlah"
        findViewById<TextView>(R.id.discount).text=": $diskonformat"
        findViewById<TextView>(R.id.total).text=": $totalformat"
    }
}