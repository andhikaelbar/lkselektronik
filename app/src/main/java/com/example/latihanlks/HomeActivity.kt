package com.example.latihanlks

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class HomeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
    }

    fun pindah_barang(view: android.view.View) {
        intent = Intent(this,MenuActivity::class.java)
        startActivity(intent)
    }
}